.. _measuring:

.. _Pandas: https://pandas.pydata.org/
.. _PyJAMAS: https://bitbucket.org/rfg_lab/pyjamas/src/master/

================
Measuring images
================

PyJAMAS_ provides tools to quantify annotated images. PyJAMAS_ can measure images currently open in the application, and it can also analyze images in batch mode for comparison of annotated datasets.

Measuring individual annotation files
=====================================

#. Open an image and the corresponding annotation file.

#. To measure the annotated objects, select *Measure polylines ...* from the **Measurement** menu.

#. Choose the measurements and the slices to include in the calculations, and select a file to save the results. The options available in *Measure polylines ...* are:

   a. **area**: units are pixels.

   b. **perimeter**: units are pixels.

   c. **pixel values**: quantifies the mean pixel values at the perimeter and inside of each polyline.

   d. **image stats**: calculates the mean and mode pixel values for each of the selected slices.

#. The measurements are saved as a CSV file, and can be opened using a spreadsheet or a text editor, as well as imported into Python (e.g. by using the read_csv function in the data processing package Pandas_) for further processing and visualization.

Batch measurements
==================

#. Organize imaging data files and their corresponding annotation files into folders. Create a folder for all data belonging to one experimental condition (for example, control). Within this folder, create a subfolder for each of the individual images belonging to the group. In each subfolder, place the greyscale image and the corresponding annotation file (or files, in case multiple structures are to be measured in an image). Make sure to include one annotation file per structure to be analyzed (e.g. one file per cell). The option to *Export individual fiducial-polyline annotations* in the **IO** menu can be used to export individual annotation files per tracked structure by clicking on a fiducial inside the corresponding polyline. Fiducials may be automatically added to the interior of each polyline using *Add seeds in polyline centroids ...* in the **Image** menu. Fiducials can then be automatically tracked.

#. Select the *Measure ...* option from the **Batch** menu. In the dialog, specify the parameters for the data group(s) to be analyzed. The analysis can be executed (with run analysis) without plotting the results. Similarly, once the analysis has been executed, the results can be plotted (with plot results) without re-running the analysis, thus saving time. The different options in the *Measure* dialog are:

   a. **time resolution**: the number of seconds between each slice for the analysis of time-lapse sequences.

   b. **xy resolution**: pixel size in microns.

   c. **images before treatment**: the number of slices in a time-lapse sequence before a treatment was applied. Set to zero if no treatment was applied.

   d. **brush size**: the width of the line used to measure the pixel values under polylines.

   e. **analyze image intensities**: if selected, not only morphological features (area, perimeter, circularity) but also pixel values will be quantified.

   f. **image extension**: the extension of the images in each subfolder.

   g. **intensity normalization**: correct for photobleaching (by dividing by the mean image intensity), background subtraction (subtracting the image mode) and photobleaching correction (as before), or neither.

   h. **analysis file name suffix**: suffix to add to the file used to store the analysis results (in csv format).

   i. **analysis file name extension**: extension of the analysis files used to store the analysis results.

   j. **error style**: for plots displaying the mean of a measurement over time, the standard error of the mean will be displayed as a band or as individual error bars.

   k. **plot style**: for plots showing the distribution of data across multiple experiments within a group, a box plot or a violin plot can be selected.

   l. **run analysis**: if selected, measurements are re-done (slower). Otherwise, previous measurement results are used.

   m. **save results**: if selected, generates and saves a csv file containing the combined results of the analysis of all the images, and a Jupyter notebook that allows reproducing the analysis using the same parameters and generate all the relevant plots. The results will be saved in the results folder. The Jupyter notebook name will consist of the date and time of the analysis followed by notebook name suffix.

   n. **save results**: if selected, generates and saves all figures, a csv file containing the combined results of the analysis of all the images, and a Python script that allows reproducing the analysis using the same parameters. The results will be saved in results folder. The Python script name will consist of the date and time of the analysis followed by script file name suffix.

   o. **plot results**: if selected, generates and saves all figures (slower).

#. The measurements for each annotation file are saved as a CSV file in the folder containing the annotation file. Overall results are saved (in csv, notebook, and/or figure format) in the results folder.

Image annotations and analysis results can be imported into Python for custom analyses and visualization.

